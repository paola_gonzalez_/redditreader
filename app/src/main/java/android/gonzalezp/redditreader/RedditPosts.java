package android.gonzalezp.redditreader;

public class RedditPosts {

    public String title;
    public String url;

    public RedditPosts(String title, String url){
        this.title = title;
        this.url = url;
    }
}
